package main

import (
	"encoding/json"
	"fmt"
)

func main() {
	prefix := "mycompany"

	key, _ := generateAPIKey(
		prefix, "", 8, 24,
	)
	keyJSON, err := json.MarshalIndent(key, "", "  ")
	if err != nil {
		fmt.Println("Whoops: %w", err)
	} else {
		fmt.Printf("%s\n", string(keyJSON))
	}

	// Store the key.longTokenHash and key.shortToken in your database and give
	// key.token to your customer.

	fmt.Println("Check API Key:", checkAPIKey(key.Token, key.LongTokenHash))
}
