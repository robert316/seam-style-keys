package main

import (
	"crypto/rand"
	"crypto/sha256"
	"errors"
	"fmt"
	"math/big"
	"strings"
)

func hashLongToken(longToken string) string {
	sum := sha256.Sum256([]byte(longToken))
	return fmt.Sprintf("%x", sum[:])
}

func checkAPIKey(token string, expectedLongTokenHash string) bool {
	return hashLongToken(extractLongToken(token)) == expectedLongTokenHash
}

func extractLongTokenHash(token string) string {
	return hashLongToken(extractLongToken(token))
}

func extractLongToken(token string) string {
	parts := strings.Split(token, "_")
	if len(parts) == 1 {
		return parts[0]
	} else if len(parts) == 0 {
		return ""
	}
	return parts[len(parts)-1]

	// return token
	// token.split("_").slice(-1)?.[0]
}

func extractShortToken(token string) string {
	parts := strings.Split(token, "_")
	if len(parts) == 1 {
		return parts[0]
	} else if len(parts) == 0 {
		return ""
	}
	return parts[1]

	// return token
	// token.split("_")?.[1]
}

func getTokenComponents(token string) *ApiKeyParts {
	return &ApiKeyParts{
		ShortToken:    extractShortToken(token),
		LongToken:     extractLongToken(token),
		LongTokenHash: extractLongTokenHash(token),
		Token:         token,
	}
}

type ApiKeyParts struct {
	ShortToken    string
	LongToken     string
	LongTokenHash string
	Token         string
}

// https://gist.github.com/dopey/c69559607800d2f2f90b1b1ed4e550fb?permalink_comment_id=3527095#gistcomment-3527095
func generateRandomString(n int) (string, error) {
	const letters = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
	generatedString := make([]byte, n)
	for i := 0; i < n; i++ {
		num, err := rand.Int(rand.Reader, big.NewInt(int64(len(letters))))
		if err != nil {
			return "", err
		}
		generatedString[i] = letters[num.Int64()]
	}

	return string(generatedString), nil
}

func generateAPIKey(
	keyPrefix string,
	shortTokenPrefix string,
	shortTokenLength int,
	longTokenLength int,
) (ApiKeyParts, error) {
	response := ApiKeyParts{}

	if keyPrefix == "" {
		return response, errors.New("keyPrefix must be set")
	}

	shortTokenStr, _ := generateRandomString(shortTokenLength)
	// Optionally encode the token string before formatting / padding:
	// bs58.encode(shortTokenStr)
	shortToken := fmt.Sprintf("%0*s", shortTokenLength, shortTokenStr)

	longTokenStr, _ := generateRandomString(longTokenLength)
	// Optionally encode the token string before formatting / padding:
	// bs58.encode(longTokenStr)
	longToken := fmt.Sprintf("%0*s", longTokenLength, longTokenStr)

	longTokenHash := hashLongToken(longToken)

	shortToken = fmt.Sprintf("%s%s", shortTokenPrefix, shortToken)[0:shortTokenLength]
	token := strings.Join([]string{keyPrefix, shortToken, longToken}, "_")

	return ApiKeyParts{
		ShortToken:    shortToken,
		LongToken:     longToken,
		LongTokenHash: longTokenHash,
		Token:         token,
	}, nil
}
