package main

import (
	"reflect"
	"testing"
)

func TestApiKeys(t *testing.T) {
	token := "mycompany_BRTRKFsL_51FwqftsmMDHHbJAMEXXHCgG"

	expectedShortToken := "BRTRKFsL"
	shortToken := extractShortToken(token)
	if shortToken != expectedShortToken {
		t.Errorf("expected short token: %s but received: %s", expectedShortToken, shortToken)
	}

	expectedLongToken := "51FwqftsmMDHHbJAMEXXHCgG"
	longToken := extractLongToken(token)
	if longToken != expectedLongToken {
		t.Errorf("expected long token: %s but received: %s", expectedLongToken, longToken)
	}

	expectedLongTokenHash := "d70d981d87b449c107327c2a2afbf00d4b58070d6ba571aac35d7ea3e7c79f37"
	longTokenHash := hashLongToken(expectedLongToken)
	if longTokenHash != expectedLongTokenHash {
		t.Errorf("expected long token hash: %s but received: %s", expectedLongTokenHash, longTokenHash)
	}

	testKey := getTokenComponents(token)
	expectedKeyValues := &ApiKeyParts{
		ShortToken:    "BRTRKFsL",
		LongToken:     "51FwqftsmMDHHbJAMEXXHCgG",
		LongTokenHash: "d70d981d87b449c107327c2a2afbf00d4b58070d6ba571aac35d7ea3e7c79f37",
		Token:         "mycompany_BRTRKFsL_51FwqftsmMDHHbJAMEXXHCgG",
	}
	if !reflect.DeepEqual(testKey, expectedKeyValues) {
		t.Errorf("unable to correctly get token components from token: %s\n"+
			"testKey: %+v\nexpectedKey: %+v", token, testKey, expectedKeyValues)
	}

	if !checkAPIKey(token, longTokenHash) {
		t.Errorf("incorrect check on api key from token: %s", token)
	}
}
