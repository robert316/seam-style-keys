### SEAM Style Keys

Used as a test to understand how the seam style keys worked based on this [example](https://github.com/seamapi/prefixed-api-key)

### Run the app

```
go run main.go api_keys.go
```

### Run the test

```
go test -v ./... -count=1
```
